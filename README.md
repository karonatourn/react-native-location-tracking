# react-native-location-tracking

Tracking location

## Installation

In `package.json` of your project, add the following line under **depedencies**

```json
"dependencies": {
    "react-native-location-tracking": "git+https://gitlab.com/karonatourn/react-native-location-tracking.git#{branch|tag}",
}
```

Replace **branch** or **tag** with the following

**branch**: master

**tag**: 0.1.0

**Recommendation**

```json
"dependencies": {
    "react-native-location-tracking": "git+https://gitlab.com/karonatourn/react-native-location-tracking.git#master",
}
```

## Additional Manual Configurations

### Android

In `AndroidManifest.xml`

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android">

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />

    <application>

      <!-- Not required -->
      <meta-data android:name="com.reactnativelocationtracking.notification_title"
        android:resource="@string/notification_title"/>
    </application>

</manifest>

```

### iOS

In `Info.plist`

```xml
<key>NSLocationWhenInUseUsageDescription</key>
<string>TEXT</string>
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
<string>TEXT</string>
<key>NSLocationAlwaysUsageDescription</key>
<string>TEXT</string>
```

In **Xcode**, select your project target > Select **Signing & Capabilities** > Add capability for **Background Nodes** > Tick for **Location updates** and **Background processing**

![alt text](./docs/images/img_xcode_config.png?raw=true)

## Usage

```js
import LocationTracking from "react-native-location-tracking";

// ...

// Start location tracking
LocationTracking.start({
    url: 'api-update-location-url',
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        // ...
    },
});

// Stop location tracking
LocationTracking.stop();
```

**Note**

You have to request location permission before start location tracking. Use [react-native-permissions](https://github.com/zoontek/react-native-permissions) to help use you with location permission requests

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
