import * as React from 'react';

import { StyleSheet, View, Button, Platform } from 'react-native';
import LocationTracking from 'react-native-location-tracking';
import { check, request, RESULTS, PERMISSIONS } from 'react-native-permissions';

export default function App() {
  const start = () =>
    LocationTracking.start({
      url: 'https://apolo-api.codingate.com/api/driver/update/location',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'language': 'en',
        'Authorize':
          'cbfcd44cb6058fa9f9a20aaacfb287ab0059e3e79495336bb434f33c9b329398',
        'Auth':
          'ce1b46e088c8bc5bc3624797b9d9c7a4593bf2f186f4c3df9d18141368ec3a96',
      },
    });

  return (
    <View style={styles.container}>
      <Button
        title="Start"
        onPress={() => {
          const permission =
            Platform.OS === 'android'
              ? PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
              : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;

          check(permission).then((result) => {
            if (result === RESULTS.GRANTED) {
              start();
            } else {
              request(permission).then((result1) => {
                if (result1 === RESULTS.GRANTED) {
                  start();
                }
              });
            }
          });
        }}
      />
      <Button
        title="Stop"
        onPress={() => {
          LocationTracking.stop();
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
