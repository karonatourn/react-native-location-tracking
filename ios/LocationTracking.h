#import <React/RCTBridgeModule.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationTracking : NSObject <RCTBridgeModule, CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;

@end
