#import "LocationTracking.h"
#import <CoreLocation/CoreLocation.h>

@implementation LocationTracking

NSString *urlString;
NSDictionary *headers;

RCT_EXPORT_MODULE()

+ (BOOL)requiresMainQueueSetup {
  return true;
}

- (dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

- (void)configure {
  if ([CLLocationManager locationServicesEnabled] && _locationManager == NULL){
    _currentLocation = [[CLLocation alloc] init];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager setPausesLocationUpdatesAutomatically:NO];
    [_locationManager requestAlwaysAuthorization];
    _locationManager.distanceFilter = 50;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager setAllowsBackgroundLocationUpdates:YES];
    [_locationManager startUpdatingLocation];
    [_locationManager startMonitoringSignificantLocationChanges];
  }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
  _currentLocation = [locations lastObject];
  [self updateDriverLocationWithlocation:_currentLocation];
}

- (void)updateDriverLocationWithlocation:(CLLocation *)location {
  if (urlString == NULL) {
    return;
  }
  
  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
  
  // Setup the request with URL
  NSURL *url = [NSURL URLWithString: urlString];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
  
  NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%f", location.coordinate.latitude], @"latitude",[NSString stringWithFormat:@"%f", location.coordinate.longitude],@"longitude", nil];
  if ([NSJSONSerialization isValidJSONObject:userDictionary]) {
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:jsonData];
    
    if (headers != NULL) {
      for (NSString *key in [headers allKeys]) {
        [urlRequest setValue:[headers objectForKey:key] forHTTPHeaderField:key];
      }
    }
  }
  
  // Create dataTask
  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

    if (error != nil) {
      NSLog(@"Error location tracking: %@", error);
    } else if (data != nil) {
      NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
      printf("Lattitude: %f",self->_currentLocation.coordinate.latitude);
      printf("Longitude: %f",self->_currentLocation.coordinate.longitude);
      BOOL status = results[@"success"];

      if (status){
        NSLog(@"updateDriverLocationWithlocation: %@", results);
      }else{
        //TODO: Handle Error
      }
    }
  }];
  
  // Fire the request
  [dataTask resume];
}

+ (id)sharedManager {
  static LocationTracking *sharedMyManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedMyManager = [[self alloc] init];
  });
  return sharedMyManager;
}

- (id)init {
  if (self = [super init]) {
    //someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
  }
  return self;
}

- (void)dealloc {
  // Should never be called, but just here for clarity really.
}


RCT_EXPORT_METHOD(start:(NSDictionary *) networkOptions) {
  urlString = [networkOptions objectForKey:@"url"];
  headers = [networkOptions objectForKey:@"headers"];
  dispatch_async(dispatch_get_main_queue(), ^{
    [self configure];
  });
}

RCT_EXPORT_METHOD(stop) {
  if (_locationManager != NULL) {
    [_locationManager stopUpdatingLocation];
    _locationManager = NULL;
  }
  
  urlString = NULL;
  headers = NULL;
}

@end
