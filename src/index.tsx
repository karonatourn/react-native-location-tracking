import { NativeModules } from 'react-native';

type LocationTrackingOption = {
  url: string;
  method?: 'POST';
  headers: HeadersInit_;
  postBody?: {
    ':lat'?: string;
    ':lng'?: string;
  };
};

type LocationTrackingType = {
  start(options: LocationTrackingOption): void;
  stop(): void;
};

const { LocationTracking } = NativeModules;

export default LocationTracking as LocationTrackingType;
