package com.reactnativelocationtracking.service;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationRequestController {

    static private final long INTERVAL = 1000;
    static private final float SMALLEST_DISPLACEMENT = 50f;
    static private final int REQUEST_FINE_LOCATION = 1000;

    private Context mContext = null;
    private LocationRequest mLocationRequest = null;
    private LocationUpdaterInterface mLocationUpdaterInterface = null;
    private FusedLocationProviderClient mFusedLocationProviderClient = null;
    private LocationCallback mLocationListener = null;
    private Location mOldLocation = null;

    public LocationRequestController(Context context) {
        mContext = context;
    }

    void startUpdates() {

        if (isHasPermissions()) {
            this.setLocationRequestProperties();
            this.startUpdate();
        } else {
            this.requestPermissions();
        }

    }

    void startUpdatesFromService() {
        if (isHasPermissions()) {
            this.setLocationRequestProperties();
            this.startUpdate();
        }
    }

    // return result to register class

    void onLocationChanged(Location location) {

        if (mLocationUpdaterInterface != null && location != null) {
            mLocationUpdaterInterface.onLocationCallBack("location", location);
        }

    }

    // remove or stop location request update

    void removeLocationUpdate() {
        mLocationUpdaterInterface = null;
        try {
            if (ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            ) {
                return;
            }

            if (mFusedLocationProviderClient != null && mLocationListener != null) {
                mFusedLocationProviderClient.removeLocationUpdates(mLocationListener);
            }


        } catch (Exception ex) {
            Log.d("On Remove", "fail to remove location listener, ignore", ex);
        }
    }

    /*===== check permission location ====*/
    private boolean isHasPermissions() {

        return (ContextCompat.checkSelfPermission(
                mContext,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(
                        mContext,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED);

    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                (Activity) mContext,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}
                , REQUEST_FINE_LOCATION
        );
    }

    void setLocationUpdateListener(LocationUpdaterInterface updateLocationListener) {
        mLocationUpdaterInterface = updateLocationListener;
    }

    boolean isBetterLocation(Location location, Location currentBestLocation) {

        if (location == null || currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > INTERVAL;
        boolean isSignificantlyOlder = timeDelta < -INTERVAL;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
        }

        // If the new location is more than two minutes older, it must be worse
        if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        boolean isNewer = timeDelta > 0L;
        float accuracyDelta = location.getAccuracy() - currentBestLocation.getAccuracy();
        boolean isLessAccurate = accuracyDelta > 0f;
        boolean isMoreAccurate = accuracyDelta < 0f;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200f;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = location.getProvider().equals(currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        } else {
            return false;
        }

    }

    private void setLocationRequestProperties() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        //Set the priority of the request.
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //automatically update time
        mLocationRequest.setInterval(INTERVAL);
        //Set the minimum displacement between location updates in meters, By default this is 0.
        mLocationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT);

    }

    private void startUpdate() {

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
        mLocationListener = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (isBetterLocation(mOldLocation, locationResult.getLastLocation()) || true) {
                    onLocationChanged(locationResult.getLastLocation());
                }
                mOldLocation = locationResult.getLastLocation();
            }
        };

        mFusedLocationProviderClient.requestLocationUpdates(
                mLocationRequest,
                mLocationListener,
                Looper.myLooper()
        );

    }

    interface LocationUpdaterInterface {
        void onLocationCallBack(String type, Location location);
    }
}
