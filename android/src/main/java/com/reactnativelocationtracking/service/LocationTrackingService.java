package com.reactnativelocationtracking.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.reactnativelocationtracking.R;
import com.reactnativelocationtracking.common.Constant;
import com.reactnativelocationtracking.common.LocationTrackingMeta;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LocationTrackingService extends Service implements LocationRequestController.LocationUpdaterInterface {
  static public final String TAG = "LocationTrackingService";
  static final private int NOTIFICATION_ID = 1337;
  static final private String NETWORK_REQUEST_TAG = "UpdateLocation";

  private LocationRequestController mLocationRequestController = null;
  private RequestQueue mQueue = null;
  private LocationTrackingOption mOptions = null;

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    boolean isStartService = intent == null || intent.getBooleanExtra(Constant.ExtraName.IS_START_SERVICE, true);

    if (intent != null) {
      mOptions = (LocationTrackingOption) intent.getSerializableExtra(Constant.ExtraName.LOCATION_TRACKING_OPTION);
    }

    if (!isStartService) {
      super.stopForeground(true);
      super.stopSelf();
    } else {
      this.startForegroundService();
    }

    return START_STICKY;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mQueue = Volley.newRequestQueue(this);
    this.startForegroundService();
    this.initLocationController();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mLocationRequestController != null) {
      this.mLocationRequestController.setLocationUpdateListener(null);
      this.mLocationRequestController.removeLocationUpdate();
      this.mLocationRequestController = null;
    }

    mQueue.cancelAll(NETWORK_REQUEST_TAG);
    this.removeNotification();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onLocationCallBack(String type, Location location) {
    mQueue.cancelAll(NETWORK_REQUEST_TAG);

    if (mOptions == null) {
      return;
    }

    String url = mOptions.getUrl();

    // Post parameters
    // Form fields and values
    HashMap<String, Double> params = new HashMap<String, Double>();
    params.put(mOptions.getLatParam(), location.getLatitude());
    params.put(mOptions.getLngParam(), location.getLongitude());
    JSONObject jsonObject = new JSONObject(params);

    // Request a string response from the provided URL.
    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
      @Override
      public void onResponse(JSONObject response) {
        if (response != null) {
          Log.d(TAG, response.toString());
        }
      }
    }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        // if (error != null) {
        //     Log.d(TAG, error.getMessage());
        // }
      }
    }) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        return mOptions.getHeaders();
      }
    };

    request.setTag(NETWORK_REQUEST_TAG);

    // Add the request to the RequestQueue.
    mQueue.add(request);

    Log.d(TAG, location.getLatitude() + "," + location.getLongitude());
  }

  private void initLocationController() {
    mLocationRequestController = new LocationRequestController(this);
    mLocationRequestController.startUpdatesFromService();
    mLocationRequestController.setLocationUpdateListener(this);
  }

  private void removeNotification() {
    String ns = Context.NOTIFICATION_SERVICE;
    NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
    nMgr.cancel(NOTIFICATION_ID);
  }

  private void startForegroundService() {

    String channelId =
      Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
        createNotificationChannel()
        : "";

    LocationTrackingMeta metaData = LocationTrackingMeta.getInstance();
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
    Resources resources = this.getResources();

    int icon = metaData.getIdRes(this, "location_tracking_icon", R.drawable.ic_location_tracking);
    String title = resources.getString(metaData.getIdRes(this, "notification_title", R.string.notification_title));

    Notification notification = notificationBuilder.setOngoing(true)
      .setSmallIcon(icon)
      .setPriority(NotificationCompat.PRIORITY_MIN)
      .setContentTitle(title)
      .setCategory(Notification.CATEGORY_SERVICE)
      .build();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      startForeground(NOTIFICATION_ID, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION);
    } else {
      startForeground(NOTIFICATION_ID, notification);
    }

  }

  @RequiresApi(Build.VERSION_CODES.O)
  private String createNotificationChannel() {
    LocationTrackingMeta metaData = LocationTrackingMeta.getInstance();
    String channelName = this.getResources().getString(metaData.getIdRes(this, "notification_channel_name", R.string.notification_channel_name));
    String channelId = Constant.Notification.CHANNEL_ID;
    NotificationChannel chan = new NotificationChannel(
      channelId, channelName,
      NotificationManager.IMPORTANCE_NONE
    );
    chan.setLightColor(Color.BLUE);
    chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
    NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    service.createNotificationChannel(chan);
    return channelId;
  }
}

