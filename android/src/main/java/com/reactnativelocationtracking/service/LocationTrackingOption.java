package com.reactnativelocationtracking.service;

import android.util.Log;

import com.facebook.react.bridge.ReadableMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LocationTrackingOption implements Serializable {
    private Map<String, String> mHeaders = null;
    private String mUrl = null;
    private String mMethod = null;

    private String mLatParam = "latitude";
    private String mLngParam = "longitude";

    public LocationTrackingOption(ReadableMap option) {
        mHeaders = new HashMap<>();

        if (option.hasKey("headers")) {
            ReadableMap headers = option.getMap("headers");
            Iterator<Map.Entry<String, Object>> iterator = headers.getEntryIterator();
            while (iterator.hasNext()) {
                Map.Entry<String, Object> value = iterator.next();
                mHeaders.put(value.getKey(), value.getValue().toString());
                Log.d("LocationTrackingOption", value.getKey() + ": " + value.getValue().toString());
            }
        }


        mUrl = option.getString("url");
        mMethod = option.getString("method");

        if (option.hasKey("postBody")) {
            ReadableMap postBody = option.getMap("postBody");
            if (postBody.hasKey(":lat")) {
                mLatParam = postBody.getString(":lat");
            }

            if (postBody.hasKey(":lng")) {
                mLngParam = postBody.getString(":lng");
            }
        }
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getMethod() {
        return mMethod;
    }

    public String getLatParam() {
        return mLatParam;
    }

    public String getLngParam() {
        return mLngParam;
    }
}
