package com.reactnativelocationtracking;

import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.UiThreadUtil;
import com.facebook.react.module.annotations.ReactModule;
import com.reactnativelocationtracking.common.Constant;
import com.reactnativelocationtracking.service.LocationTrackingOption;
import com.reactnativelocationtracking.service.LocationTrackingService;

@ReactModule(name = LocationTrackingModule.NAME)
public class LocationTrackingModule extends ReactContextBaseJavaModule {
  public static final String NAME = "LocationTracking";
  private ReactApplicationContext mContext;

  public LocationTrackingModule(ReactApplicationContext reactContext) {
    super(reactContext);
    mContext = reactContext;
  }

  @Override
  @NonNull
  public String getName() {
    return NAME;
  }

  @ReactMethod
  public void start(ReadableMap networkOptions) {
    UiThreadUtil.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Intent intent = new Intent(mContext, LocationTrackingService.class);
        intent.putExtra(Constant.ExtraName.IS_START_SERVICE, true);
        intent.putExtra(Constant.ExtraName.LOCATION_TRACKING_OPTION, new LocationTrackingOption(networkOptions));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
          mContext.startForegroundService(intent);
        } else {
          mContext.startService(intent);
        }
      }
    });
  }

  @ReactMethod
  public void stop() {
    UiThreadUtil.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Intent intent = new Intent(getCurrentActivity(), LocationTrackingService.class);
        intent.putExtra(Constant.ExtraName.IS_START_SERVICE, false);
        mContext.stopService(intent);
      }
    });
  }
}
