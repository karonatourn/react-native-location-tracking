package com.reactnativelocationtracking.common;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class LocationTrackingApi {

  public class Location {
    public double latitude;
    public double longitude;

    public Location(double latitude, double longitude) {
      this.latitude = latitude;
      this.longitude = longitude;
    }
  }

  public interface LocationTrackingWebService {
    @POST()
    Call<Location> updateLocation( );
  }
}
