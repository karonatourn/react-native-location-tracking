package com.reactnativelocationtracking.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.IdRes;

public class LocationTrackingMeta {
  static private LocationTrackingMeta instance = new LocationTrackingMeta();
  private final String META_PREFIX = "com.reactnativelocationtracking.";

  private LocationTrackingMeta() {
  }

  public static LocationTrackingMeta getInstance() {
    return instance;
  }

  private Bundle getMetaData(Context context) {
    try {
      PackageManager packageManager = context.getPackageManager();

      if (packageManager == null) return null;

      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(
        context.getPackageName(),
        PackageManager.GET_META_DATA
      );

      if (applicationInfo != null) return applicationInfo.metaData;
    } catch (PackageManager.NameNotFoundException exception) {
      // do nothing
    }

    return null;
  }

  public int getIdRes(Context context, String key, int defaultValue) {
    Bundle metaData = getMetaData(context);
    if (metaData == null) return defaultValue;
    return metaData.getInt(META_PREFIX + key, defaultValue);
  }

  public String getStringValue(Context context, String key, String defaultValue) {
    Bundle metaData = getMetaData(context);
    if (metaData == null) return defaultValue;
    return metaData.getString(META_PREFIX + key, defaultValue);
  }
}
