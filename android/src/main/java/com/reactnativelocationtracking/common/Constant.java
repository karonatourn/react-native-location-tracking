package com.reactnativelocationtracking.common;

public final class Constant {
    static public class ExtraName {
        static public final String IS_START_SERVICE = "start_service";
        static public final String LOCATION_TRACKING_OPTION = "location_tracking_option";
    }

    static public class Notification {
      static public final String CHANNEL_ID = "com.reactnativelocationtracking.LocationTrackingNotification";
    }
}
